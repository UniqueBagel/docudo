﻿using DocuDo.Data.DataStorage;
using DocuDo.Data.DocumentDos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DocuDo.Pages.CreatedDocuments
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreateDocument : Page
    {
        // false = create, true = edit.
        private bool _editMode = false;
        private CreatedDocument _currentDocument;

        public CreateDocument()
        {
            this.InitializeComponent();
        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(titleTextBox.Text) && !String.IsNullOrWhiteSpace(contentTextBox.Text))
            {
                CreatedDocument document = new CreatedDocument(titleTextBox.Text, contentTextBox.Text);

                if (!_editMode)
                    StaticStorage.GenStorage.addDocument(document);
                else
                    StaticStorage.GenStorage.replaceDocument(_currentDocument, document);

                this.Frame.Navigate(typeof(DocumentsViewer));
            }

            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var document = e.Parameter as CreatedDocument;

            if (document != null)
            {
                createButton.Content = "Edit";
                _editMode = true;
                _currentDocument = document;

                titleTextBox.Text = document.Title;
                contentTextBox.Text = document.Content;
            }
        }

    }
}
