﻿using DocuDo.Data.DataStorage;
using DocuDo.Data.DocumentDos;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace DocuDo.Pages.CreatedDocuments
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DocumentsViewer : Page
    {

        public GeneralStorage DocumentStorage { get { return StaticStorage.GenStorage; } }

        public DocumentsViewer()
        {
            this.InitializeComponent();

            //Debug.WriteLine("I've got {0} entries", DocumentStorage.DocumentStorage.Documents.Count().ToString());
        }

        private void addNewDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(CreateDocument));
        }

        private void openDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            CreatedDocument document = (CreatedDocument)(sender as FrameworkElement).Tag;

            if (document != null)
            {
                this.Frame.Navigate(typeof(CreateDocument), document);
            }
        }

        private void removeDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            CreatedDocument document = (CreatedDocument)(sender as FrameworkElement).Tag;

            if (document != null)
            {
                DocumentStorage.removeDocument(document);
            }
        }
    }
}
