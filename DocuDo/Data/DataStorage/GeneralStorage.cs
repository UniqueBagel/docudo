﻿using DocuDo.Data.DocumentDos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuDo.Data.DataStorage
{
    public class GeneralStorage: Storage<CreatedDocumentManager>
    {

        public CreatedDocumentManager DocumentStorage { get { return (CreatedDocumentManager)this.StorageObject; } }

        public GeneralStorage(): base("genstorage.dd")
        {
            
        }

        internal void addDocument(CreatedDocument document)
        {
            if (DocumentStorage.addDocument(document))
            {
                saveStorage();
            }
            
        }

        internal void replaceDocument(CreatedDocument old_document, CreatedDocument new_document)
        {
            if (DocumentStorage.removeDocument(old_document) && DocumentStorage.addDocument(new_document))
            {
                saveStorage();
            }
        }

        public bool removeDocument(CreatedDocument document)
        {
            return DocumentStorage.removeDocument(document);
        }
    }
}
