﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuDo.Data.DocumentDos
{
    public class CreatedDocumentLeaf : CreatedDocument
    {
        public CreatedDocumentLeaf(string Title, string Content) : base(Title, Content)
        {

        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(this.Parent.ToString());

            builder.Append(String.Format("Document Title: {0} \n" +
                                 "Content: {1} \n", this.Title, this.Content));

            return builder.ToString();
        }
    }
}
