﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DocuDo.Data.DocumentDos
{
    public class CreatedDocumentManager
    {
        [XmlArray("Documents")]
        [XmlArrayItem("CreatedDocument", Type = typeof(CreatedDocument))]
        public ObservableCollection<CreatedDocument> Documents { get; set; } = new ObservableCollection<CreatedDocument>();

        public CreatedDocumentManager()
        {

        }

        public bool addDocument(CreatedDocument document)
        {
            if (!Documents.Contains(document))
            {
                Documents.Add(document);
                return true;
            }

            return false;
        }

        public bool removeDocument(CreatedDocument document)
        {
            return Documents.Remove(document);
        }

        
    }

}
