﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuDo.Data.DocumentDos
{
    public class CreatedDocument
    {
        public string Title { get; set; }

        public string Content { get; set; }

        // Tree possibilities

        private CreatedDocument _parent;
        public CreatedDocument Parent { get { return _parent; } set { _parent = value; } }

        private List<CreatedDocument> _childeren = new List<CreatedDocument>();

        public List<CreatedDocument> Childeren { get { return _childeren; } private set { _childeren = value; } }

        public CreatedDocument(string Title, string Content)
        {
            this.Title = Title;
            this.Content = Content;
        }

        public virtual CreatedDocument getChild(int index)
        {
            if (_childeren.Count() > index)
            {
                return _childeren[index];
            }
            else
            {
                throw new NullReferenceException(String.Format("Cannot return, since array isn't atleast of size {0}"));
            }

        }

        public virtual void addChild(CreatedDocument document)
        {
            _childeren.Add(document);
        }

        public override string ToString()
        {

            StringBuilder builder = new StringBuilder();

            if (this.Parent != null)
            {
                builder.Append("Parent "+ this.Parent.ToString());
            }

            builder.Append("Document: " + this.Title + " \n" +
                         "Content: " + this.Content + "\n");

            foreach (CreatedDocument document in Childeren)
            {
                builder.Append(document.ToString());
            }

            return builder.ToString();
        }

        public override bool Equals(object obj)
        {
            if (!String.IsNullOrWhiteSpace(this.Title))
            {

            } else if (!String.IsNullOrWhiteSpace(this.Content))
            {

            }

            return false;
        }

        // DONT USE (PARAMLESS CONSTRUCTOR FROM XML SERIALIZATION)
        private CreatedDocument()
        {

        }
    }
}
