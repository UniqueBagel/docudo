﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocuDo.Data.DocumentDos
{

    public static class CreatedDocumentSearcher
    {
        public static List<CreatedDocument> getDocumentsStartingWith(List<CreatedDocument> documents, string startingWith)
        {
            return documents.Where(doc => doc.Title.StartsWith(startingWith)).ToList();
        }

        public static List<CreatedDocument> getDocumentsContaining(List<CreatedDocument> documents, string containing)
        {
            return documents.Where(doc => doc.Title.StartsWith(containing)).ToList();
        }

        
    }
}
